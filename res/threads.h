#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

pthread_mutex_t mutexQueue;

void* threadHandler(void* args) {
    while (1) {
        pthread_mutex_lock(&mutexQueue);
        pthread_mutex_unlock(&mutexQueue);

        for (int i = 0; i < 100; i++) { }
        printf("The number is %d\n", rand() % 100);
    }
}

int main(int argc, char* argv[]) {
    pthread_t th;
    pthread_mutex_init(&mutexQueue, NULL);

    if (pthread_create(&th, NULL, &threadHandler, NULL) != 0) {
        perror("Failed to create the thread");
    }

    srand(time(NULL));
    while (1) {
        printf("active\n");
        usleep(10000000);
        pthread_mutex_lock(&mutexQueue);
        printf("not active\n");
        usleep(10000000);
        pthread_mutex_unlock(&mutexQueue);
    }

    if (pthread_join(th, NULL) != 0) {
        perror("Failed to join the thread");
    }

    pthread_mutex_destroy(&mutexQueue);
    return 0;
}
