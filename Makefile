# Globals-
SRC=src
BIN=bin

# Compiler
CXX=g++

# CI/CD
clean:
	rm -rf $(BIN) react_server reactor.os preactor.os

# dist folders
$(BIN):
	mkdir $(BIN)

# units
$(BIN)/reactor.o: $(SRC)/reactor.cpp
	g++ -fPIC -c $(SRC)/reactor.cpp -o $(BIN)/reactor.o

$(BIN)/react_server.o: $(SRC)/react_server.cpp
	g++ -fPIC -c $(SRC)/react_server.cpp -o $(BIN)/react_server.o

$(BIN)/preactor.o: $(SRC)/preactor.cpp
	g++ -fPIC -c $(SRC)/preactor.cpp -o $(BIN)/preactor.o

$(BIN)/preact_server.o: $(SRC)/preact_server.cpp
	g++ -fPIC -c $(SRC)/preact_server.cpp -o $(BIN)/preact_server.o

# applications
reactor.os: $(BIN) $(BIN)/reactor.o
	g++ -fPIC -shared -o reactor.os $(BIN)/reactor.o

preactor.os: $(BIN) $(BIN)/preactor.o
	g++ -fPIC -shared -o preactor.os $(BIN)/preactor.o

react_server: $(BIN) $(BIN)/react_server.o reactor.os preactor.os
	g++ -fPIC -o react_server $(BIN)/react_server.o ./reactor.os ./preactor.os

# build
all: react_server

rebuild: clean all

# tests
start-react_server: rebuild
	./react_server

test-1:
	telnet 127.0.0.1 9034

test-2:
	sleep 3
	nc 127.0.0.1 9034 -q 1 < tests/message.txt
	echo "single message 1" | nc 127.0.0.1 9034 -q 1
	nc 127.0.0.1 9034 -q 1 < tests/message.txt
	echo "single message 2" | nc 127.0.0.1 9034 -q 1
