# os-assignment-4

Assignment 4 for "Operating Systems"

* For more information, you can see in the PDF in the docs directory.
* In addition, it supports to add new ports and stopping the server (Waiting...) and "exit" command for exit the server.

## Requirements

* make
* g++

## Build

For install this tools run the following command in the terminal:

```bash
make all
```

Or if is already built

```bash
make rebuild
```

## Get Started

For open the server run the following command in the terminal:

```bash
./react_server
```

## Testing

Before testing the tool make share that the server is running.\
Now, you can run the following commands:

```bash
telnet 127.0.0.1 9034 < tests/message.txt
echo "single message" | telnet 127.0.0.1 9034
telnet 127.0.0.1 9034 < tests/message.txt
echo "single message" | telnet 127.0.0.1 9034
telnet 127.0.0.1 9034
```

## Customization

You can choose the app that will compile by src/build.h
If BASIC_APP is 1, then it will compile with simple demo like in task.
If Basic_APP is 0, it will compile with the full app.

## Links

* Thread Pool Idea: <https://code-vault.net/lesson/j62v2novkv:1609958966824>

## Authors

- Omer Priel

## License

MIT
