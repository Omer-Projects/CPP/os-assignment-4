#include "reactor.h"

#include <vector>
#include <unordered_map>
#include <algorithm>
#include <pthread.h>
#include <poll.h>

#include "build.h"

// private
class Reactor {
public:
    std::vector<int> m_fds;
    std::vector<struct pollfd> m_pollFds;
    std::unordered_map<int, handler_t> m_handlers;
    bool m_started = false;
    bool m_running = false;
    pthread_t m_thread = 0;
    pthread_mutex_t m_lock{};
};

static void* main_loop(void *args) {
    auto reactor = (Reactor*)args;

    while (true) {
        pthread_mutex_lock(&reactor->m_lock);
        pthread_mutex_unlock(&reactor->m_lock);

        int error = poll(reactor->m_pollFds.data(), reactor->m_pollFds.size(), POLL_TIMEOUT * 30);
        if (error == -1) {
            ERROR("poll");
        }

        for (auto item : reactor->m_pollFds) {
            if (item.revents & POLLIN) {
                (*(reactor->m_handlers[item.fd]))(reactor, item.fd);
            }
        }
    }

    return args;
}

// public
void* createReactor()
{
    return (void*)(new Reactor());
}

void destroyReactor(void* reactorPtr)
{
    auto reactor = (Reactor*)reactorPtr;

    if (reactor->m_started) {
        pthread_mutex_destroy(&reactor->m_lock);
        pthread_cancel(reactor->m_thread);
    }

    delete (Reactor*)reactor;
}

bool isRunningReactor(void* reactor)
{
    return ((Reactor*)reactor)->m_running;
}

unsigned long sizeReactor(void* reactor)
{
    return ((Reactor*)reactor)->m_fds.size();
}

void addFd(void* reactorPtr, int fd, handler_t handler)
{
    auto reactor = (Reactor*)reactorPtr;

    if (std::find(reactor->m_fds.begin(), reactor->m_fds.end(), fd) != reactor->m_fds.end()) {
        reactor->m_handlers[fd] = handler;
    } else {
        pollfd pollFd{};
        pollFd.fd = fd;
        pollFd.events = POLLIN;
        pollFd.revents = 0;

        reactor->m_fds.push_back(fd);
        reactor->m_pollFds.push_back(pollFd);
        reactor->m_handlers[fd] = handler;
    }
}

void removeFd(void* reactorPtr, int fd)
{
    auto reactor = (Reactor*)reactorPtr;

    auto found = std::find(reactor->m_fds.begin(), reactor->m_fds.end(), fd);
    if (found != reactor->m_fds.end()) {
        long index = found - reactor->m_fds.begin();

        reactor->m_fds.erase(found, found);
        reactor->m_pollFds.erase(reactor->m_pollFds.begin() + index, reactor->m_pollFds.begin() + index);

        reactor->m_handlers.erase(fd);
    }
}

void startReactor(void* reactorPtr)
{
    auto reactor = (Reactor*)reactorPtr;

    if (reactor->m_running) {
        return;
    }

    if (!reactor->m_started) {
        reactor->m_started = true;
        pthread_mutex_init(&reactor->m_lock, nullptr);
        if (pthread_create(&reactor->m_thread, nullptr, &main_loop, (void*)reactor) != 0) {
            ERROR("Failed to create the thread");
        }
    } else {
        pthread_mutex_unlock(&reactor->m_lock);
    }
    reactor->m_running = true;
}

void stopReactor(void* reactorPtr)
{
    auto reactor = (Reactor*)reactorPtr;

    if (reactor->m_running) {
        pthread_mutex_lock(&reactor->m_lock);
        reactor->m_running = false;
    }
}

void waitFor(void* reactorPtr)
{
    auto reactor = (Reactor*)reactorPtr;

    if (reactor->m_running) {
        if (pthread_join(reactor->m_thread, nullptr) != 0) {
            ERROR("Failed to join the thread");
        }
    }
}
