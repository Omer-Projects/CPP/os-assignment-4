#pragma once

typedef void (*handler_preactor_t)(int fd);

void* createPreactor();
void destroyPreactor(void* preactor);

int runPreactor(void* preactor);
int cancelPreactor(void* preactor);

unsigned long sizePreactor(void* preactor);

void addFD2Preactor(void* preactor, int fd, handler_preactor_t handler);
void removeHandler(void* preactor, int fd);
