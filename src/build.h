#pragma once

#include <iostream>

#define BASIC_APP 0

#define DEFAULT_PORT "9034"
#define MAX_CLIENTS 1024
#define BUFFER_SIZE 1024
#define POLL_TIMEOUT 100

#define ERROR(message) std::cout << "ERROR: " << message << std::endl
#define INFO(message) std::cout << "INFO: " << message << std::endl
#define DEBUG(message) std::cout << "DEBUG: " << message << std::endl
