/**
 * @brief Entry Point to react_server
 */

#include <iostream>
#include <unistd.h>
#include <string>

#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "build.h"
#include "reactor.h"
#include "preactor.h"

#define COMMAND_WAIT "wait"
#define COMMAND_EXIT "exit"
#define MESSAGE_FOR_CLIENT "got message from client!\n"

static char s_buffer[BUFFER_SIZE];
static void* s_preactor;

void creatSocketListener(void* reactor, const char* port);
void listenerHandler(void* reactor, int fd);
void clientHandler(void* reactor, int fd);
void preactorHandler(int fd);

// beej server

void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void creatSocketListener(void* reactor, const char* port)
{
    int listenerFd;
    int yes=1, rv;

    addrinfo hints, *ai, *p;

    // get us a socket and bind it
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if ((rv = getaddrinfo(nullptr, port, &hints, &ai)) != 0) {
        ERROR("selectserver: " << gai_strerror(rv));
        return;
    }

    for(p = ai; p != nullptr; p = p->ai_next) {
        listenerFd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (listenerFd < 0) {
            continue;
        }

        // lose the pesky "address already in use" error message
        setsockopt(listenerFd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

        if (bind(listenerFd, p->ai_addr, p->ai_addrlen) < 0) {
            close(listenerFd);
            continue;
        }

        break;
    }

    // if we got here, it means we didn't get bound
    if (p == nullptr) {
        ERROR("selectserver: failed to bind to the port " << port);
        return;
    }

    freeaddrinfo(ai); // all done with this

    // listen
    if (listen(listenerFd, MAX_CLIENTS) == -1) {
        ERROR("listen");
        close(listenerFd);
        return;
    }

    INFO("New Listener on port " << port);
    addFd(reactor, listenerFd, &listenerHandler);
}

void listenerHandler(void* reactor, int fd)
{
    char remoteIP[INET6_ADDRSTRLEN];

    sockaddr_storage remoteaddr;
    socklen_t addrlen = sizeof remoteaddr;
    int clientfd = accept(fd,
                          (struct sockaddr *)&remoteaddr,
                          &addrlen);

    if (clientfd == -1) {
        ERROR("accept");
        return;
    }

    const char* fromIP = inet_ntop(remoteaddr.ss_family,
                                   get_in_addr((struct sockaddr*)&remoteaddr),
                                   remoteIP, INET6_ADDRSTRLEN);
    INFO("new connection from " << fromIP << " on " << clientfd);

    addFd(reactor, clientfd, &clientHandler);
    addFD2Preactor(s_preactor, clientfd, &preactorHandler);
}

void clientHandler(void* reactor, int fd)
{
    int nbytes = recv(fd, s_buffer, BUFFER_SIZE, 0);
    if (nbytes < 0) {
        ERROR("recv");
    } else if (nbytes == 0) {
        INFO("selectserver: socket " << fd << " hung up");
        close(fd);
        removeFd(reactor, fd);
        removeHandler(s_preactor, fd);
    } else {
        std::cout << s_buffer;
        if (s_buffer[strlen(s_buffer) - 1] != '\n') {
            std::cout << "\n";
        }

        runPreactor(s_preactor);
    }
}

void preactorHandler(int fd) {
    send(fd, MESSAGE_FOR_CLIENT, strlen(MESSAGE_FOR_CLIENT), 0);
}

int main()
{
    INFO("react_server stating");

    void* reactor = createReactor();
    s_preactor = createPreactor();

    creatSocketListener(reactor, DEFAULT_PORT);

    startReactor(reactor);

#if BASIC_APP
    waitFor(reactor);
#else
    unsigned long lastSize = sizeReactor(reactor);

    bool running = true;
    while (running) {
        std::cin.getline(s_buffer, BUFFER_SIZE);
        std::string command = std::string(s_buffer);
        if (std::string(s_buffer) == std::string(COMMAND_EXIT)) {
            INFO("Start exit...");
            running = false;
            stopReactor(reactor);
        } else {
            if (!isRunningReactor(reactor)) {
                INFO("Continue");
                startReactor(reactor);
            } else if (command == std::string(COMMAND_WAIT) || command.empty()) {
                stopReactor(reactor);
                INFO("Waiting ... ");
            } else {
                int port = -1;
                try {
                    port = std::stoi(command);
                } catch (const std::invalid_argument &ex) {
                    ERROR("The command \"" << command << "\" not supported!");
                }

                if (port != -1) {
                    creatSocketListener(reactor, command.c_str());
                }
            }
        }
        if (lastSize != sizeReactor(reactor)) {
            lastSize = sizeReactor(reactor);
            INFO("Now connected to the reactor " << lastSize << " handlers");
        }
    }

    waitFor(reactor);

    INFO("Exit");
#endif

    destroyReactor(reactor);
    destroyPreactor(s_preactor);

    return 0;
}
