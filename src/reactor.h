#pragma once

typedef void (*handler_t)(void* reactor, int fd);

void* createReactor();
void destroyReactor(void* reactor);

void startReactor(void* reactor);
void stopReactor(void* reactor);

bool isRunningReactor(void* reactor);
unsigned long sizeReactor(void* reactor);

void addFd(void* reactor, int fd, handler_t handler);
void removeFd(void* reactor, int fd);

void waitFor(void* reactor);
