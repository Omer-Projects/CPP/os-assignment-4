#include "preactor.h"

#include <vector>
#include <unordered_map>
#include <algorithm>
#include <pthread.h>
#include <poll.h>

#include "build.h"

// private
class Preactor {
public:
    std::unordered_map<int, handler_preactor_t> m_handlers;
    pthread_t m_thread = 0;
};

static void* main_loop(void *args) {
    auto preactor = (Preactor*)args;

    for (auto& it: preactor->m_handlers) {
        it.second(it.first);
    }

    return args;
}

// public
void* createPreactor()
{
    return (void*)(new Preactor());
}

void destroyPreactor(void* preactorPtr)
{
    auto preactor = (Preactor*)preactorPtr;

    delete (Preactor*)preactor;
}

unsigned long sizePreactor(void* preactor)
{
    return ((Preactor*)preactor)->m_handlers.size();
}

void addFD2Preactor(void* preactorPtr, int fd, handler_preactor_t handler)
{
    auto preactor = (Preactor*)preactorPtr;

    preactor->m_handlers[fd] = handler;
}

void removeHandler(void* preactorPtr, int fd)
{
    auto preactor = (Preactor*)preactorPtr;

    preactor->m_handlers.erase(fd);
}

int runPreactor(void* preactorPtr)
{
    auto preactor = (Preactor*)preactorPtr;

    if (pthread_create(&preactor->m_thread, nullptr, &main_loop, (void*)preactor) != 0) {
        ERROR("Failed to create the thread");
        return 1;
    }
    return 0;
}

int cancelPreactor(void* preactorPtr)
{
    auto preactor = (Preactor*)preactorPtr;

    if (pthread_cancel(preactor->m_thread) != 0) {
        ERROR("Failed to cancel the thread");
        return 1;
    }

    return 0;
}
